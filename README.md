# quick-former

前端开发无可避免要遇到表单设计的问题，特别是一些和流程、报表、统计相关的需求时，可动态自定义设计的表单尤为重要。这样就能根据当前需求，设计出最符合的表单，用来收集数据。现在介绍一下摸索出来的表单设计器，采用的是 Vue,ElementUI 为大框架技术，还用到了 Vue.draggable,vue-json-viewer,lodash 等。

### 项目命令

```
npm install
npm run serve
```

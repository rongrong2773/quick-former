import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'element-ui/lib/theme-chalk/index.css'
import ElementUI from 'element-ui'
Vue.use(ElementUI)

import '@/components/style/index.scss'
import Components from '@/components/index.js'
Vue.use(Components)

Vue.config.productionTip = false

const appInstantiate = new Vue({
  router,
  store,
  render: (h) => h(App),
})
// window.vm = appInstantiate//组件得实例全局调用得时候使用
appInstantiate.$mount('#app')
